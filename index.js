const csv = require('csv-parser');
const fs = require('fs');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;
const csvWriter = createCsvWriter({
  path: 'tempRaport.csv',
  header: [
    { id: 'kb', title: 'kb' },
    { id: 'label', title: 'label' },
    { id: 'question', title: 'question' },
    { id: 'response', title: 'response' },
    { id: 'URL', title: 'URL' },
    { id: 'cnt', title: 'cnt' },
    { id: 'grp_id', title: 'grp_id' },
  ],
});

let finalData = [];
const urlRegex = /(http(s)?:\/\/.)(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z@]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g;
const doubleBR = /<br><br>/g
const replaceHTML = (data) => {
  const html = /<[^>]*>/g;
  if (typeof data === 'string') {
    const newdata = data.replace(html, '');
    return newdata;
  } else return 'Błędny format danych';
};
let count = 2;

const Formater = () => {

    fs.createReadStream('raport.csv')
      .pipe(csv())
      .on('data', (row) => {
        row.response = row.response.replace(/;/g, '');
        row.response = row.response.replace(doubleBR,'\n')
        const url = row.response.match(urlRegex);
        if (url) {
          url.forEach((link) => {
            const isEmail = link.includes('@');
            if (!isEmail) {
              row.response = row.response.replace(link, '');
              row.URL = row.URL ? row.URL + '\n' +  link : link;
            } else {
              row.URL = '-';
            }
          });
        } else {
          row.URL = '-';
        }
        finalData.push(row);
      })
      .on('end', () => {
        csvWriter
          .writeRecords(finalData)
          .then(() => {
                       fs.readFile('./tempRaport.csv', 'utf8', function (err, plik) {
    if (err) {
      return console.log(err);
    }
    const oldString = plik;
    fs.writeFileSync('./raportOut.csv', replaceHTML(oldString));
  });
          console.log('Nowe csv wygenerowane');
          })
   
      });
      
};

Formater();
